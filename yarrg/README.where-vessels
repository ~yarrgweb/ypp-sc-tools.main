			where-vessels
			=============

where-vessels displays a map of the ocean with your ships marked on it.


USAGE:
======

    $ cd .../ypp-sc-tools/yarrg
    $ ./where-vessels [options]

    Then, in your YPP client:
      * go to the docks of any island
      * click on the button "Where are my vessels"
      * click anywhere in the main part of the screen
      * press Ctrl-A Ctrl-C

    In where-vessels, click "Acquire"

You may well want to create a "vessel-notes" file to get the best
use out of where-vessels, see below.

If you don't have exactly one YPP client open and logged in then you
will have to pass --ocean and --pirate options.

If there was trouble acquiring the data (or loading the notes, see
below) you can click on the error message to open a window giving
information about the problem.


OPTIONS:
========

 --ocean OCEAN
     Use map of OCEAN.  Default is to guess by looking for an
     open Puzzle Pirates client on the same display (logged on
     as PIRATE, if --pirate was used).

 --pirate PIRATE
     Supplies the name of your pirate.  If you also supply a notes
     file (see below) where-vessels will treat ships belonging to
     PIRATE as your own.  An empty argument means do not care about
     vessel ownership.  Default is not to care about ownership without
     notes; with notes, default is to look for a Puzzle Pirates client
     on the same display (on OCEAN, if --ocean was used).

 --notes SOURCE
     Use SOURCE as the vessel notes.  See below.
     SOURCE may be of the following forms:
       empty argument            No notes, this is the default
       SCHEME:... eg http:...    URL, fetched with Tcl's http package
       |COMMAND ARG ARG          command (Tcl list) whose output is the notes
       NOTES-FILE                ordinary file

 --filter-separate-lock-owner
     In the filter, show two separate 3-button filters for the lock
     status, and the ship owner, rather than one 9-button filter with
     an option for each combination.  (If there are no notes, only
     one 3-button filter, for the lock status, is shown, anyway.)

 --vessel-info-source RSYNC-SRC
     Update information about vessels, subclasses, icons, etc. from
     RSYNC-SRC.  Default is yarrg.chiark.net::yarrg/vessel-info.
     If RSYNC-SRC is the empty string, do not update and just use
     local data from icons/* and _vessel_info_cache.

 --clipboard-file FILE
     Use FILE as the initial clipboard containing a set of vessels to
     display, rather than waiting for Acquire.  Useful for testing or
     if you want to save your vessel locations for when you're not
     logged in.  The easiest way to create a suitable FILE is
     xclipboard

 --local-html-dir DIR
     Do not fetch ocean map from Yppedia.  Instead, use copy in DIR.


USER INTERFACE:
===============

There are some things that won't be obvious from the user interface.

The black square with the blue square inside it is the panner for the
map.

If you get an error message from Acquire, or a message about the
notes, shown as a red, yellow or blue message below the Acquire or
Reload Notes button, you can click on the message to get a pop-up
window with the full error message.

Colonised islands are shown with names in black; uncolonised in grey.
Dotted routes are unpurchaseable charts.  (Information from Yppedia.)

If you click on one of the little white icons on the map, you'll see
an explanation of what that icon string means appear in the top right.
Each element of the icon is explained in turn from left to right.
Then you'll see a list of the ships represented below, ordered by
owning pirate (if known) and then by name.

The "Show" panel allows you to filter the displayed ships.  If you
don't select "These:" then none of the other controls in the "Show"
panel have any effect, so touching any of the latter sets the overall
filtering style back to "These:".

"Copy island names" makes where-vessels "copy" (as in "copy and
paste") a comma-separated list of the names of the islands at which
selected ships are present.  You can then paste this into other tools
(eg, the YARRG website).  (Technically, "copy island names" claims the
X Primary Selection.)

"Display/combine details" allows the map display to be decluttered by
squashing together differing ships:
 * "Size round down": selecting a size means that every ship of that
   size will be rounded down to the next un-selected size.  Naturally
   you cannot select sloops for rounding down!
 * "Lock/owner": "Yours?" simply pretends that all unknown ships are
   not yours; that is useful if your crew has many ships and you only
   bother writing about your own ships in the notes.  "For you" shows
   the vessel lock status, except that if it's your ship and unlocked
   it shows just that it's yours (and doesn't show whether it's
   unlocked or crew use).  "Lock" shows the lock status only.
 * Flags: See perlop(1) on the tr/// (aka y///) operator.  All
   characters entered into the boxes are literal; ranges etc. are not
   supported.
Filtering done by the "Show" panel takes effect before the collapsing
done by "Display/combine details", so you can filter on details which
you are collapsing for display.

If you select (click on) one of the ship names in the list in the top
right, you'll get an icon string below the vessel list which gives the
complete information for that vessel, as it was before the collapsing.



VESSEL NOTES:
=============

The information collected via the clipboard from the Yohoho Puzzle
Pirates client does not include any information about who owns the
ship.  (Sadly, it doesn't even include whether the ship is yours, even
though this can be seen on the screen by looking at whether the icon
has a blue fringe.)

It also doesn't include any information about how the ship is stocked,
what might be recorded on the Officer Notice Board about how it is to
be used, etc.

So to help you make sense of your fleet, where-vessels can read a file
of notes about each ship.  The lines in this file are of the form:
  <vessel-id> <name> = <owning pirate name> [<flags>]

If your vessel-notes file doesn't mention a ship, you can click on the
message "1 warning(s)" below "Reload notes" and it will open a window
showing what the problem is.  This will include, for ships missing
from the notes, a template line for each ship, eg:
    # Eta Island:
    1730081   High Silverside               =
Copy and paste the line with the ship name to your vessel-notes file
and fill it in, eg:
    1730081   High Silverside               =  Anaplian   T
You probably don't want to copy the line with the island name into
your vessel-notes; it's just there to help you identify and find the
ship in question (for example if you want to look at it in-game), but
it will become out of date if the ship moves.

Blank lines are ignored, as are lines starting with #.

It is best if you can arrange to have a single notes file for the
whole crew, and find some what to edit it collectively.

Flags:
------

The "flags" field has nothing to do with the in-game political entity
known as a Flag.  It's a mostly free-form text field, with the
following properties:
 - It is displayed next to each ship on the map (so ships with
    non-identical flags are not grouped and counted)
 - You can filter ships by entering a Perl regexp on the flags
 - It may not contain spaces

It is therefore usually best to use the flags field for one-letter
codes referring to the properties of each ship.  Here is an example,
from the Special Circumstances shared vessel-notes file:

# Don't edit if you're not logged into Puzzle Pirates.
# To avoid simultaneous clashing edits, please coordinate with
# your other Fleet Officers using the in-game chat.

# Flags for the column after the owner.
#
#  L	Ship is public special use.  Eg the chart library.
#
#  S	Ship is personal special use.  Eg, personal storage
#	for a stall or personal trading.
#
#  P	Ship is stocked and otherwise suitable for pillaging etc:
#	It is normally kept well stocked with rum and cannonballs and
#	doesn't mind much where it's left.
#
#  T	Ship is stocked and otherwise suitable for trading or memming:
#	It is normally kept stocked with a little rum but few if any
#	cannonballs.  It doesn't mind much where it's left.
#
#  G	Ship is often used for the ad-hoc storage of commodities eg
#	as part of trading activities.
#
#  R	Ship is borrowable but with some restrictions; ONB has details.
#
#  A	Ship is an auxiliary vessel (eg a supply sloop) or normally
#	sails as part of a convoy.
#
#  DO NOT rely on this file to be up to date.  Check the Officer
#  Notice Board before borrowing a ship.
