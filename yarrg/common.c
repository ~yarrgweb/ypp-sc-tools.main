/*
 * Utility functions
 */
/*
 *  This is part of ypp-sc-tools, a set of third-party tools for assisting
 *  players of Yohoho Puzzle Pirates.
 * 
 *  Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *  Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 *  are used without permission.  This program is not endorsed or
 *  sponsored by Three Rings.
 */

#include "common.h"

void *mmalloc(size_t sz) {
  void *r;
  if (!sz) return 0;
  sysassert( r= malloc(sz) );
  return r;
}
void *mcalloc(size_t sz) {
  void *r;
  if (!sz) return 0;
  sysassert( r= malloc(sz) );
  memset(r, 0, sz);
  return r;
}
void *mrealloc(void *p, size_t sz) {
  assert(sz);
  void *r;
  sysassert( r= realloc(p,sz) );
  return r;
}

DEFINE_VWRAPPERF(, progress, )
DEFINE_VWRAPPERF(, progress_log, )
DEFINE_VWRAPPERF(, progress_spinner, )
DEFINE_VWRAPPERF(, warning, )
DEFINE_VWRAPPERF(, fatal, NORET)

static int last_progress_len;

static void vprogress_core(int spinner, const char *fmt, va_list al) {
  int r;
  
  if (o_quiet) return;
  if (!debug_flags && !isatty(2)) return;
  
  if (last_progress_len)
    putc('\r',stderr);

  r= vfprintf(stderr,fmt,al);

  if (spinner) {
    putc(spinner,stderr);
    r++;
  }

  if (r < last_progress_len) {
    fprintf(stderr,"%*s", last_progress_len - r, "");
    if (!r) putc('\r', stderr);
    else while (last_progress_len-- > r) putc('\b',stderr);
  }
  last_progress_len= r;

  if (ferror(stderr) || fflush(stderr)) _exit(16);
}
   
void vprogress(const char *fmt, va_list al) { vprogress_core(0,fmt,al); }
void vprogress_spinner(const char *fmt, va_list al) {
  static const char spinchars[]="/-\\";
  static int spinner;

  vprogress_core(spinchars[spinner],fmt,al);
  spinner++;
  spinner %= (sizeof(spinchars)-1);
}

void vprogress_log(const char *fmt, va_list al) {
  if (o_quiet) return;
  
  progress("");
  vfprintf(stderr,fmt,al);
  putc('\n',stderr);
  fflush(stderr);
}

void vwarning(const char *fmt, va_list al) {
  progress("");
  fputs("Warning: ",stderr);
  vfprintf(stderr,fmt,al);
  fputs("\n",stderr);
  fflush(stderr);
}

void vfatal(const char *fmt, va_list al) {
  progress("");
  fputs("\n\nFatal error: ",stderr);
  vfprintf(stderr,fmt,al);
  fflush(stderr);
  fputs("\n\n",stderr);
  _exit(4);
}

void sysassert_fail(const char *file, int line, const char *what) {
  int e= errno;
  progress("");
  fprintf(stderr,
	  "\nfatal operational error:\n"
	  " unsuccessful execution of: %s\n"
	  " %s:%d: %s\n\n",
	  what, file,line, strerror(e));
  _exit(16);
}

void waitpid_check_exitstatus(pid_t pid, const char *what, int sigpipeok) { 
  pid_t got;
  int st;
  for (;;) {
    got= waitpid(pid, &st, 0);
    if (pid==-1) { sysassert(errno==EINTR); continue; }
    break;
  }
  sysassert( got==pid );

  if (WIFEXITED(st)) {
    if (WEXITSTATUS(st))
      fatal("%s failed with nonzero exit status %d",
	    what, WEXITSTATUS(st));
  } else if (WIFSIGNALED(st)) {
    if (!sigpipeok || WTERMSIG(st) != SIGPIPE)
      fatal("%s died due to signal %s%s", what,
	    strsignal(WTERMSIG(st)), WCOREDUMP(st)?" (core dumped)":"");
  } else {
    fatal("%s gave strange wait status %d", what, st);
  }
}

char *masprintf(const char *fmt, ...) {
  char *r;
  va_list al;
  va_start(al,fmt);
  sysassert( vasprintf(&r,fmt,al) >= 0);
  sysassert(r);
  va_end(al);
  return r;
}

unsigned debug_flags;

void debug_flush(void) {
  sysassert(!ferror(debug));
  sysassert(!fflush(debug));
}
