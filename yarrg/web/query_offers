<%doc>

 This is part of the YARRG website.  YARRG is a tool and website
 for assisting players of Yohoho Puzzle Pirates.

 Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 Copyright (C) 2009 Clare Boothby

  YARRG's client code etc. is covered by the ordinary GNU GPL (v3 or later).
  The YARRG website is covered by the GNU Affero GPL v3 or later, which
   basically means that every installation of the website will let you
   download the source.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 are used without permission.  This program is not endorsed or
 sponsored by Three Rings.


 This Mason component generates the core of the `offers' query.


</%doc>
<%args>
$quri
$dbh
$commodid => undef;
$commodstring => '';
$islandid => undef;
$prselector
$someresults
$emsgokorprint
</%args>

<%perl>
my $emsg;
my @warningfs;
my @islandids;
my @archipelagoes;
my ($commodname,$cmid);

my $qa= \%ARGS;
</%perl>

<div class="query">
<h1>Prices for commodity at location(s)</h1>

% $prselector->('BuySell');

<form action="<% $quri->() |h %>" method="get">

<& enter_commod, qa => $qa, dbh => $dbh, emsg_r => \$emsg,
	commodname_r => \$commodname,
	cmid_r => \$cmid
 &>

<& enter_route, qa => $qa, dbh => $dbh, emsg_r => \$emsg,
	warningfs_r => \@warningfs,
	enterwhat => 'Enter location',
	islandids_r => \@islandids,
	archipelagoes_r => \@archipelagoes
 &>

<input type=submit name=submit value="Go">
% my $ours= sub { $_[0] =~
%    m/^commodstring|^commodid|^routestring|^archipelago|^island/;
% };
<& "lookup:formhidden", ours => $ours &>

</form>
</div>
<div class="results">

%#========== results ==========
<%perl>

$emsgokorprint->($emsg) or $cmid=undef;
print("</div>"), return
	unless defined $cmid and @islandids;

foreach my $wf (@warningfs) { $wf->(); }

if ($qa->{'debug'}) {
</%perl>
<pre>
bs= <% $qa->{BuySell} %>
cmdid= <% $cmid %>
islandids= <% join ',', map { defined($_) ? $_ : 'U' } @islandids %>
</pre>
<%perl>
}

my $locdesc;
if (@islandids>1) {
	$locdesc= ' at specified locations';
} elsif (defined $islandids[0]) {
	my $sth= $dbh->prepare("SELECT islandname FROM islands
				 WHERE islandid == ?");
	$sth->execute($islandids[0]);
	$locdesc= ' at '.($sth->fetchrow_array())[0];
} else {
	$locdesc= ' in '.$archipelagoes[0];
}

my $now= time;

my @conds;
my @condvals;
push @condvals, $cmid;
foreach my $ix (0..$#islandids) {
	my $iid= $islandids[$ix];
	my $arch= $archipelagoes[$ix];
	if (defined $iid) {
		push @conds, 'offers.islandid == ?';
		push @condvals, $iid;
	} else {
		push @conds, 'islands.archipelago == ?';
		push @condvals, $arch;
	}
}
foreach my $bs (split /_/, $qa->{BuySell}) {
	my %da_ages;
	my %ts_sortkeys;

	die unless grep { $bs eq $_ } qw(buy sell);
	my $ascdesc= $bs eq 'buy' ? 'DESC' : 'ASC';
</%perl>
<h2>Offers to <% uc $bs |h %> <% $commodname |h %> <% $locdesc %></h2>
<%perl>
	my $stmt= "
	    SELECT	archipelago, islandname,
			stallname, price, qty, timestamp,
			offers.stallid
		FROM $bs AS offers
		JOIN islands ON offers.islandid==islands.islandid
		JOIN uploads ON offers.islandid==uploads.islandid
		JOIN stalls ON offers.stallid==stalls.stallid
		WHERE offers.commodid == ?
                 AND ( ".join("
		    OR ", @conds)."
		     )
		ORDER BY archipelago, islandname, price $ascdesc, qty ASC,
			stallname $ascdesc
";
	if ($qa->{'debug'}) {
</%perl>
<pre>
<% $stmt %>
<% join ',', @condvals |h %>
</pre>
<%perl>
	}

	my $row;
	my $sth= $dbh->prepare($stmt);
	$sth->execute(@condvals);
	my $rowix= 0;
</%perl>
%	while ($row= $sth->fetchrow_arrayref) {
%		if (!$rowix) {
<table class="data" id="<% $bs %>_table" rules=groups>
<colgroup span=2>
<colgroup span=3>
<colgroup span=1>
<tr>
<th>Archipelago
<th>Island
<th>Stall or Shoppe
<th>Price
<th>Quantity
<th>Data age
</tr>
%		}
%		my $rowid= ${bs}.$row->[6];
%		my $tscellid= "c$rowid";
%		my $age= $now - $row->[5];
%		$da_ages{$rowid}= $age;
%		$row->[5]= 
<tr id=<% $rowid %> class="<% 'datarow'.($rowix & 1) %>" >
%		foreach my $ci (0..4) {
%			my $val= $row->[$ci];
%			$ts_sortkeys{$ci}{$rowid}= $val;
<td <% $ci >= 3 ? 'align=right' : '' %> ><% $val |h %>
%		}
<td id="<% $tscellid %>" align=right><% prettyprint_age($age) %>
</tr>
%		$rowix++;
%	}
%	if ($rowix) {
</table>

<&| tabsort, table => "${bs}_table", rowclass => 'datarow', cols => [
	{}, {}, {},
	{ Numeric => 1, DoReverse => 1 },
	{ Numeric => 1, DoReverse => 1 },
	{ Numeric => 1, DoReverse => 1, SortKey => "${bs}_ages[rowid]" }],
	sortkeys => "${bs}_sortkeys"
  &>
  <%$bs%>_sortkeys= <% to_json_protecttags(\%ts_sortkeys) %>;
  <%$bs%>_ages= <% to_json_protecttags(\%da_ages) %>;
</&tabsort>
%	} else {
No offers.
%	}

<%perl>
}
</%perl>

<p>
(Please don't use these pages to scrape data out of the YARRG
database.  This will be a pain for you to program, slow to run, and
pointlessly overload our server.  Instead, see our
<a href="devel">information for developers</a>
to find out how to get testing data or a real-time feed.)

</div>
