<%doc>

 This is part of the YARRG website.  YARRG is a tool and website
 for assisting players of Yohoho Puzzle Pirates.

 Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 Copyright (C) 2009 Clare Boothby

  YARRG's client code etc. is covered by the ordinary GNU GPL (v3 or later).
  The YARRG website is covered by the GNU Affero GPL v3 or later, which
   basically means that every installation of the website will let you
   download the source.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 are used without permission.  This program is not endorsed or
 sponsored by Three Rings.


 This Mason component generates the core of the `data age' query.


</%doc>

<%args>
$quri
$dbh
</%args>

<%once>

</%once>

<%perl>
my $now= time;

my $sth= $dbh->prepare("SELECT archipelago, islandid, islandname, timestamp
				FROM uploads JOIN islands USING (islandid)
				ORDER BY archipelago, islandname");
$sth->execute();

</%perl>
<div class="results">

<& SELF:pageload &>
<%method pageload>
<&| script &>
  da_pageload= Date.now();
</&script>
</%method>

<h1>Market data age</h1>

<& SELF:agestable, now => $now, fetchrow => sub { $sth->fetchrow_hashref } &>

<%method agestable>
<%args>
  $now
  $fetchrow
</%args>
<table class="data" id="ts_table" rules="groups">
<tr>
<th>Archipelago
<th>Island
<th>Age
% my $row;
% my %da_ages;
% my %ts_sortkeys;
% $da_ages{'id_loaded'}= 0;
% my $rowix= 0;
% while ($row= $fetchrow->()) {
%	my $rowid= "id_$row->{'islandid'}";
%	my $cellid= "c$rowid";
%	my $age= $now - $row->{'timestamp'};
%	$ts_sortkeys{'0'}{$rowid}= $row->{'archipelago'};
%	$ts_sortkeys{'1'}{$rowid}= $row->{'islandname'};
%	$da_ages{$rowid}= $age;
<tr id=<% $rowid %> class="<% 'datarow'.($rowix & 1) %>"
   > <td><% $row->{'archipelago'} |h
  %> <td><% $row->{'islandname'} |h
  %> <td id="<% $cellid %>" align=right><% prettyprint_age($age) %> </tr>
%	$rowix++;
% }
</table>
<& SELF:dataages, id2age => \%da_ages,
	jsprefix => 'dat_', elemidprefix => "'c'+" &>

<&| tabsort, table => 'ts_table', rowclass => 'datarow', cols => [
	{}, {},
	{ DoReverse => 1,
	  Numeric => 1,
	  SortKey => "dat_ages[rowid]" }]
  &>
  ts_sortkeys= <% to_json_protecttags(\%ts_sortkeys) %>;
</&tabsort>
<p>
% print $m->content();
Time since this page loaded:
<span id="cid_loaded">(not known; times above not updating)</span>

</%method>

<%method dataages>
<%args>
  $id2age
  $elemidprefix => ''
  $jsprefix => 'da_'
</%args>
<&| script &>
  function <% $jsprefix %>Refresh() {
    var now= Date.now();
    debug('updating now='+now);
    for (var ageid in <% $jsprefix %>ages) {
      if (!<% $jsprefix %>ages.hasOwnProperty(ageid)) continue;
      var oldage= <% $jsprefix %>ages[ageid];
      var el= document.getElementById(<% $elemidprefix %>ageid);
      var age= oldage + (now - da_pageload) / 1000;
      var newhtml= <% meta_prettyprint_age('age','Math.floor','+') %>;
      el.innerHTML= newhtml;
    }
  }
  <% $jsprefix %>ages= <% to_json_protecttags($id2age) %>;
  window.setInterval(<% $jsprefix %>Refresh, 10000);
  register_onload(<% $jsprefix %>Refresh);
</&>
</%method>

<form action="lookup" method="get">
<input type=submit name=submit value="Reload">
<& "lookup:formhidden", ours => sub { 0; } &>
</form>

</div>
<%init>
use POSIX;
use CommodsWeb;
</%init>
