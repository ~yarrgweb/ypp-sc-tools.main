<%doc>

 This is part of the YARRG website.  YARRG is a tool and website
 for assisting players of Yohoho Puzzle Pirates.

 Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 Copyright (C) 2009 Clare Boothby

  YARRG's client code etc. is covered by the ordinary GNU GPL (v3 or later).
  The YARRG website is covered by the GNU Affero GPL v3 or later, which
   basically means that every installation of the website will let you
   download the source.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 are used without permission.  This program is not endorsed or
 sponsored by Three Rings.


 This Mason component generates the introduction.


</%doc>
<& docshead &>
<h1>YARRG development, contribution and troubleshooting</h1>

<h2>Free Software (aka Open Source) licensing</h2>

YARRG is Free Software - you may share and modify it.  See the
licences for details.  Not only the client but also the website code
is Free.  The yarrg client, support files, and so forth are under the
GNU GPL (v3 or later); the website and database backend is under the
GNU Affero GPL (v3 or later).

<p>

Note that there is <strong>NO WARRANTY</strong> !

<h2>Resources for developers</h2>

<h3>Source code</h3>

The website and upload client are maintained at
<a href="https://gitlab.com/yarrg"><kbd>https://gitlab.com/yarrg</kbd></a>.

<p><a href="source.tar.gz">Live website source code</a> can
be downloaded:  In case we have made changes but not yet pushed them
(perhaps because we haven't done a release), and to make it easy for
anyone else who runs a copy of the website to provide everyone with
the source for their version, the website code itself lets you download
an up-to-date of its actually-running source code.  So this link
gives you the source code for the pages you are now looking at.

<h4>Uploader</h4>

The uploader is also Free Software, but under a slightly different
licence.  The <a href="upload">uploader page</a> has
<a href="upload#source">complete information on how to get its source code</a>.

<h3>YARRG website code instances</h3>

<h4>Lookup website</h4>
 <ul>
 <li><a href="http://yarrg.chiark.net/">Main released site</a>,
     with <a href="http://yarrg.chiark.net/source.tar.gz">source code</a> (and
   <a href="git://git.yarrg.chiark.net/ypp-sc-tools.web-live.git">committed changes via git</a>)
 <li><a href="http://yarrg.chiark.net/test/data">Released code; testing database instance (updated with yarrg --test-servers)</a> (source code as above)
 <li><a href="http://yarrg.chiark.net/test/code">Testing version of the site code</a>,
     with <a href="http://yarrg.chiark.net/test/code/source.tar.gz">source code</a> (and
   <a href="git://git.yarrg.chiark.net/ypp-sc-tools.web-test.git">committed changes via git</a>)
 <li><a href="http://yarrg.chiark.net/test/both">Testing code running against the testing database instance</a> (source code as above)
 </ul>

<p>

You can add the form parameter <kbd>?debug=1</kbd> to each of the
website URLs above to get a version of the site with debugging output enabled.

<h4>Database and dictionary update receiver</h4>

On the live system there are different trees, potentially with
different versions, for receiving updates and managing the database,
to the ones for displaying the website.  The database management trees
are normally accessed only via the upload client but you may also
download their source directly:

 <ul>
 <li><a href="http://upload.yarrg.chiark.net/commod-update-receiver?get_source=1">Main production instance</a> including uncommitted changes; or
   <a href="git://git.yarrg.chiark.net/ypp-sc-tools.db-live.git">committed changes via git</a>
 <li><a href="http://upload.yarrg.chiark.net/test/commod-update-receiver?get_source=1">Testing database instance</a> including uncommitted changes; or
   <a href="git://git.yarrg.chiark.net/ypp-sc-tools.db-test.git">committed changes via git</a>
 </ul>

<h3>Data</h3>

<kbd>rsync rsync.yarrg.chiark.net::yarrg/</kbd><br>
accesses files published for the benefit of the old yarrg upload client
and other members of the ypp-sc-tools family.

<p>

This directory also contains slightly stale copies (updated daily)
of the actual databases (in SQLite3 format).
It is <a href="http://yarrg.chiark.net/RSYNC/">also available by http</a>
but please do not repeatedly download the databases by http - use
rsync which is much more efficient.  Note also that if you want to
actually run your own improved yarrg website, I can arrange to
feed you data in real time - see below.

<p>

<kbd>rsync rsync.yarrg.chiark.net::yarrg/test/</kbd><br>
accesses the data for the testing instance.

<h3>Documentation</h3>

<a href="http://www.chiark.greenend.org.uk/~ijackson/ypp-sc-tools/master/yarrg/README.devel">README.devel</a>
has the specification of the mechanism and format for uploading to YARRG.

<h2>Support from the YARRG team</h2>

If there are problems, considering filing a ticket
<a href="https://gitlab.com/yarrg/ypp-sc-tools/-/issues">on gitlab</a>
Merge requests are very welcome.

<p>

If you would like to run a (perhaps modified) copy of the YARRG
website it would be very easy for us to make our system send you
copies of updates submitted by users of the YARRG clients including
JARRG, in the format expected by the YARRG code.  Please just
ask us - at our end it's just a matter of us adding your database
instance's special email address to our alias file.

<p>

We encourage the development and improvement of this code.  Please
continue to share your improvements with the Puzzle Pirates community.
In particular, do not remove or break the feature that allows users of
your website to download the up to date code you are running.

<h2>Contacting the YARRG developers</h2>

Email Ian Jackson ijackson (at) chiark.greenend.org.uk.  Or talk to
any Fleet Officer or above of the crew Special Circumstances on the
Cerulean Ocean.

<p>
</div>
<& footer, isdevel => 1 &>
